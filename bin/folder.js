#! /usr/bin/env node
const args = process.argv.slice(2);
const fs = require("fs");
const path = require("path");

console.log("⚛ Creating Wil folder structure for React-ts!");

const folderNameList = [
  "components",
  "components/Button",
  "components/Nav",
  "components/Input",
  "components/Form",
  "styles",
  "pages",
  "views",
  "constants",
  "routes",
  "assets",
  "assets/images",
  "interfaces",
  "services",
  "hooks",
  "context",
  "utils"
];

try {
  folderNameList.forEach((folderNameItem) => {
    const folderName = path.resolve(process.cwd()) + "/src/" + folderNameItem;
    if (!fs.existsSync(folderName)) {
      fs.mkdirSync(folderName);
    }
  });
} catch (err) {
  console.error(err);
}
