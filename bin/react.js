#! /usr/bin/env node
const args = process.argv.slice(2);
const { exec } = require("child_process");
const fs = require("fs");
const path = require("path");

// caution : in development don't use this //

const createReactApp = async (ts, appName) => {
  if (ts) {
    exec(
      `npx create-react-app ${appName} --template typescript`,
      (error, stdout, stderr) => {
        if (error) {
          console.log(`error: ${error.message}`);
          return;
        }
        if (stderr) {
          console.log(`stderr: ${stderr}`);
          return;
        }
        console.log(`${stdout}`);
      }
    );
  } else {
    exec(`sudo npx create-react-app ${appName}`, (error, stdout, stderr) => {
      if (error) {
        console.log(`error: ${error.message}`);
        return;
      }
      if (stderr) {
        console.log(`stderr: ${stderr}`);
        return;
      }
      console.log(`${stdout}`);
    });
  }
};

const createWillReactApp = async (ts, appName) => {
  console.log("🚂 ...create-wil-react-app is running");
  switch (appName) {
    case ".":
      await createReactApp(ts, appName);
      break;
    default:
      await createReactApp(ts, appName);
      break;
  }
};

try {
  if (args.length < 1) {
    throw new Error("Please enter your react project name");
  } else if (args.length > 2) {
    throw new Error("Unknown arguments");
  } else if (args.length === 2) {
    if (args[1] === "-ts") createWillReactApp(true, args[0]);
    else throw new Error("Invalid arguments");
  } else {
    createWillReactApp(false, args[0]);
  }
  process.exit(0);
} catch (error) {
  console.error(error);
  process.exit(1);
}

// caution : in development don't use this //